/*
Player
represents player (blue triangle) in game
*/

class Player {
  
  //three corner points for the triangle, starting from the top point going clockwise.
  PVector p1, p2, p3;

  //speed of the player in the x and y direction
  float speed = 4;
  //player will be rendered in teal
  color c = color(66, 245, 245);

  //there will be a cooldown for shooting, and number of bullets on the window will also be limited
  float bulletMax = 5, shootingCooldown, shootingCooldownMax = 0.1;

  //we will store all our bullets in this ArrayList
  ArrayList<Bullet> bullets;
  
  //constructor for main class
  Player() {
   //initialize bullets ArrayList
   bullets = new ArrayList<Bullet>();
   
   p1 = new PVector(250, 700);
   p2 = new PVector(230, 750);
   p3 = new PVector(270, 750);
   
   shootingCooldown = shootingCooldownMax;
  }
  
  //this will be used to see how much the player moves 
  void update(float x, float y) {
   float newX = x * speed;
   float newY = y * speed;
   
   if (p2.x + newX <= 0 || p3.x + newX >= 500) {
      newX = 0;
    } 

    if (p1.y + newY <= 0 || p2.y + newY >= 800) {
      newY = 0;
    }
    
    //assemble a PVector from the calculated offsets for ease of use
    PVector pSpeed = new PVector(newX, newY);

    //add the speed PVector to all points to move them by the offset on both axes 
    p1 = PVector.add(p1, pSpeed);
    p2 = PVector.add(p2, pSpeed);
    p3 = PVector.add(p3, pSpeed);
    
    //reduce shootingCooldown. we are using 1/frameRate to use second as the unit for the cooldown values 
    shootingCooldown -= (1/ frameRate);
    
    //process each bullet. movethem, then render them if they are within bounds of the screen, or remove them from the ArrayList if they are not
    for (int i = 0; i < bullets.size(); i++) {
      Bullet b = bullets.get(i);
      if (b.move()) {
        b.render();
      } else {
        bullets.remove(i);
      }
    }
  }
  
  //render the player
  void render() {
    noStroke();
    fill(c);

    triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y); //player will be a triangle
  }
  
  //this function returns an array of PVectors, to be used in collision checks
  PVector[] getVertices() {
    //To be used with the collision function
    return new PVector[]{p1, p2, p3};
  }
  
  //if cooldown period has passed, and there are less than max amount of bullets on the screen, spawn a new bullet and add it to the arraylist, then reset cooldown.
  void shoot() {
    if (shootingCooldown <= 0 && bullets.size() < bulletMax) {
      bullets.add(new Bullet(p1));
      shootingCooldown = shootingCooldownMax;
    }
  }
}
