/*
Shoot'em'up style interactive, object oriented toy
Main sketch will handle input and main frame
*/

//GameManager object will contain most of our game logic
GameManager gm;

void setup() {
  size(500, 800);
  //start the game with 7 enemies
  gm = new GameManager(7);
}

void draw() {
  //will not do anything if game over
  if (gm.gameOver)
    return;

  //clear the screen and call Game Manager's update and draw functions
  background(0);
  gm.update();
  gm.render();
}


//Handle keyPress. Will set keys value in GameManager to true for the key pressed
void keyPressed() {
  if (keyCode == 32 || keyCode == 10) //Spacebar or Enter
  {
    gm.keys[0] = true;
  }

  if (keyCode == 37 || keyCode == 65) //Left Arrow or A
  {
    gm.keys[1] = true;
  }

  if (keyCode == 38 || keyCode == 87 ) //Up Arrow or W
  {
    gm.keys[2] = true;
  }

  if (keyCode == 39 || keyCode == 68) //Right Arrow or D
  {
    gm.keys[3] = true;
  }
  if (keyCode == 40 || keyCode == 83) //Down Arrow or S
  {
    gm.keys[4] = true;
  }
}


//Handle keyRelease. Will set keys value in GameManager to false for the key released
void keyReleased() {

  if (keyCode == 32 || keyCode == 10) //Spacebar or Enter
  {
    gm.keys[0] = false;
  }

  if (keyCode == 37 || keyCode == 65) //Left Arrow or A
  {
    gm.keys[1] = false;
  }

  if (keyCode == 38 || keyCode == 87 ) //Up Arrow or W
  {
    gm.keys[2] = false;
  }

  if (keyCode == 39 || keyCode == 68) //Right Arrow or D
  {
    gm.keys[3] = false;
  }

  if (keyCode == 40 || keyCode == 83) //Down Arrow or S
  {
    gm.keys[4] = false;
  }
}
